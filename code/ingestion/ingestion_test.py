''' Test script for reading the TF record created with ADNI sMRI volumes.'''
import tensorflow as tf
import numpy as np
from ..utils import adni_tfexample

data_path = '/home/yogatheesan/Desktop/AGING/code/test_data/test_tfrecords'  # address to tfrecord file

with tf.Session() as sess:
    
    # Create a list of filenames and pass it to a queue
    filename_queue = tf.train.string_input_producer([data_path], num_epochs=1)
    # Define a reader and read the next record
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)

    # Parse the example
    _, _, gender, _, image_id, depth, height, width, vectorized_volume = adni_tfexample.parse_adni_example(serialized_example)

    # Reshape the image to a format that can be used for training
    image = tf.reshape(vectorized_volume, [1, depth, height, width, -1])

    # Initialize all global and local variables
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)

    # Create a coordinator and run all QueueRunner objects
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    # Run TF session
    img, lbl = sess.run([image, gender])

    # Print label
    print('Female' if lbl == 0 else 'Male' + ' : ' + str(img.shape))

    # Stop the threads
    coord.request_stop()

    # Wait for threads to stop
    coord.join(threads)
    sess.close()