'''
Python script to load and ingest sMRI volumes as tensorflow examples.

To run this (from ~/Desktop/AGING):

python code/ingestion/adni_ingestion.py \
--data_dir='/home/yogatheesan/Desktop/AGING/code/test_data/' \
--outfile_name='/home/yogatheesan/Desktop/AGING/code/test_data/test_tfrecords' \
--input_csv='/home/yogatheesan/Desktop/AGING/code/test_data/test_labels.csv'

'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import glob
import logging
import numpy as np
import pandas as pd
import os
import re
from scipy.ndimage import zoom
import sys
import tensorflow as tf
from tensorflow import gfile
from tensorflow.app import flags
from ..utils import medical_volume as medvol
from ..utils import adni_tfexample

FLAGS = flags.FLAGS

flags.DEFINE_string('data_dir', '', 'Absolute path to the directory where images are located')
flags.DEFINE_string('outfile_name', 'test_tfrecords', 'Output file name including absolute path')
flags.DEFINE_string('input_csv', '', 'Absolute path to the CSV file containing labels')

_CLINICAL_STAGE_NUMERIC_MAP = {
    'CN': 1,  # Cognitively Normal
    'MCI': 2,  # Mild Cognitive Impairment
    'AD': 3  # Alzheimer's Disease
}  # A mapping from string clinical stages to numerics

_GENDER_NUMERIC_MAP = {
    'F': 0,
    'M': 1
}  # A mapping from string gender label to numerics


def write_tfrecord(img_names, outfile_name, input_csv):
	''' This method writes the images given in img_names as TF Records.

	Args:
		img_names: list containing image names.
		outfile_name: name of the output TF Record file.
		input_csv: name of the CSV file containing label information.'''

	# open the TFRecords file.
	writer = tf.python_io.TFRecordWriter(outfile_name)
	
	for i, image_name in enumerate(img_names):

		# Find image id from image name.
		match = re.search(r'\d+.nii$', image_name)
		image_id = match.group()[:-4]

		if not (gfile.Exists(input_csv)):
			return

		label_csv = pd.read_csv(gfile.Open(input_csv))

		# Extract labels from CSV file.
		clinical_stage = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Group'].map(_CLINICAL_STAGE_NUMERIC_MAP).values[0]
		age = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Age'].values[0]
		gender = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Sex'].map(_GENDER_NUMERIC_MAP).values[0]
		subject_id = label_csv.loc[label_csv.iloc[:, 0] == int(image_id), 'Subject'].values[0]
		
		# Load image.
		mv = medvol.MedicalVolume()
		if mv.load_nifti_volume(image_name):
			img = mv.data

			# Resize image to the actual physical dimension
			img = zoom(img, (mv.pixdims[0], mv.pixdims[1], mv.pixdims[2]))

			img = img.reshape(1, img.shape[0], img.shape[1], img.shape[2], -1)

			example = adni_tfexample.create_adni_example(
				clinical_stage=clinical_stage,
				age=age,
				gender=gender,
				subject_id=subject_id,
				image_id=image_id,
				depth=img.shape[1],
				height=img.shape[2],
				width=img.shape[3],
				vectorized_volume=img.reshape(-1)
				)

			print('Exporting image ' + image_id)

			# Serialize to string and write on the file.
			writer.write(example.SerializeToString())
		else:
			print('Could not load file ' + image_name)

	writer.close()
	sys.stdout.flush()

def main(unused_argv):

	all_files = glob.glob(os.path.join(FLAGS.data_dir, '*.nii'))
	write_tfrecord(all_files, FLAGS.outfile_name, FLAGS.input_csv)


if __name__ == '__main__':
	tf.app.run()