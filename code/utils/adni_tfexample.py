'''Util for creating and reading TF Examples based on ADNI images.'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf

def _bytes_feature(value):
	return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _floatlist_feature(value):
	return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def _int64_feature(value):
	return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def create_adni_example(clinical_stage, age, gender, subject_id, image_id, depth, height, width, vectorized_volume):
	''' Creates a TF Example using image and additional features.'''

	feature = {
	'AD/clinical_stage/value': _int64_feature(int(clinical_stage)),
	'Subject/age/value': _int64_feature(int(age)),
	'Subject/gender/value': _int64_feature(int(gender)),
	'Subject/ID': _bytes_feature(subject_id.encode()),
	'Image/ID': _bytes_feature(image_id.encode()),
	'volume/depth': _int64_feature(depth),
	'volume/height': _int64_feature(height),
	'volume/width': _int64_feature(width),
	'volume/decoded': _bytes_feature(tf.compat.as_bytes(vectorized_volume.tostring()))
	}

    # Return an example protocol buffer.
	return tf.train.Example(features=tf.train.Features(feature=feature))

def parse_adni_example(serialized_adni_example):
	''' Parses an ADNI TF Example and returns image and additional features.
	(Should be run within a TF session.)
	'''

	feature = {
	'AD/clinical_stage/value': tf.FixedLenFeature([], tf.int64),
	'Subject/age/value': tf.FixedLenFeature([], tf.int64),
	'Subject/gender/value': tf.FixedLenFeature([], tf.int64),
	'Subject/ID': tf.FixedLenFeature([], tf.string),
	'Image/ID': tf.FixedLenFeature([], tf.string),
	'volume/depth': tf.FixedLenFeature([], tf.int64),
	'volume/height': tf.FixedLenFeature([], tf.int64),
	'volume/width': tf.FixedLenFeature([], tf.int64),
	'volume/decoded': tf.FixedLenFeature([], tf.string)
	}

	# Decode the record read by the reader
	features = tf.parse_single_example(serialized_adni_example, features=feature)

	# Retrieve individual attributes
	clinical_stage = tf.cast(features['AD/clinical_stage/value'], tf.int32)
	age = tf.cast(features['Subject/age/value'], tf.int32)
	gender = tf.cast(features['Subject/gender/value'], tf.int32)
	depth = tf.cast(features['volume/depth'], tf.int32)
	height = tf.cast(features['volume/height'], tf.int32)
	width = tf.cast(features['volume/width'], tf.int32)
	subject_id = tf.cast(features['Subject/ID'], tf.string)
	image_id = tf.cast(features['Image/ID'], tf.string)
	vectorized_volume = tf.decode_raw(features['volume/decoded'], tf.float64)
    
	return clinical_stage, age, gender, subject_id, image_id, depth, height, width, vectorized_volume

