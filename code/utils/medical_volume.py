from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import zlib
import numpy as np
import struct
from tensorflow import gfile
import logging


_NIFTI_DATATYPE_MAP = {
    0: ('unknown', 'x', 4),
    1: ('bool', '?', 0.25),
    2: ('unsigned char', 'B', 1),
    4: ('signed short', 'h', 2),
    8: ('signed int', 'i', 4),
    16: ('float', 'f', 4),
    32: ('complex', 'x', 8),
    64: ('double', 'd', 8),
    128: ('RGB' 'B', 3),
    256: ('signed char', 'b', 1),
    512: ('unsigned short', 'H', 2),
    768: ('unsigned int', 'I', 4),
    1024: ('long long', 'q', 8),
    1280: ('unsigned long long', 'Q', 8),
    1536: ('long double', 'd', 16),
    1792: ('double pair', 'd', 16),
    2048: ('long double pair', 'd', 32),
    2304: ('RGBA', 'B', 4)
}


class MedicalVolume(object):
  """Class to load and manipulate multi-dimensional images."""

  def __init__(self):
    self.number_dims = 0
    self.data_type = np.dtype('uint')
    self.dims = None
    self.pixdims = None
    self.data = None
    self.original_file = ''

  def load_nifti_volume(self, file_name):
    """Load NIFTI file into class.

    Args:
      file_name: the file to load.
    Returns:
      A boolean value indicating successful loading.
    """
    # append extension if not present.
    if file_name[-4:] != '.nii':
      file_name += '.nii'

    # returns False if the file doesn't exist
    if not (gfile.Exists(file_name)):
      logging.error('Non existent file')
      return False

    with gfile.GFile(file_name, 'rb') as f:
      raw_data = f.read()

    # returns False if the file format is not NIFTI
    sizeof_hdr = struct.unpack('>i', raw_data[0:4])[0]
    if(sizeof_hdr != 348):
      logging.error('Not a nifti file!')
      return False

    self.original_file = file_name
    self.number_dims = struct.unpack('>h', raw_data[40:42])[0]
    self.dims = np.zeros(self.number_dims, np.dtype(np.int))
    self.pixdims = np.zeros(self.number_dims, np.dtype(np.float))

    self.dims[2] = struct.unpack('>h', raw_data[42:44])[0]
    self.dims[1] = struct.unpack('>h', raw_data[44:46])[0]
    self.dims[0] = struct.unpack('>h', raw_data[46:48])[0]

    self.pixdims[2] = struct.unpack('>f', raw_data[80:84])[0]
    self.pixdims[1] = struct.unpack('>f', raw_data[84:88])[0]
    self.pixdims[0] = struct.unpack('>f', raw_data[88:92])[0]

    datatype_code = struct.unpack('>h', raw_data[70:72])[0]
    dt_tup = _NIFTI_DATATYPE_MAP[datatype_code]
    self.data_type = dt_tup[0]

    total_size = self.dims[0] * self.dims[1] * self.dims[2]
    image_data = struct.unpack('>' + (dt_tup[1] * total_size),
                               raw_data[348:348 + dt_tup[2] * total_size])

    self.data = np.asarray(image_data).reshape(self.dims[0], self.dims[1],
                                               self.dims[2])

    return True